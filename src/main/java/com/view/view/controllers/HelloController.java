package com.view.view.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/hello")
public class HelloController {

	@GetMapping(value="")
	public String add(Model model,@RequestParam(name="name",required=false) String name) {
		model.addAttribute("name",name);
		return "hello";
	}
}
